FROM ghcr.io/ublue-os/silverblue-nvidia:latest AS base

# Remove Firefox, install repos and enable automatic updates.
RUN rpm-ostree override remove firefox firefox-langpacks && \
rpm --import https://packages.microsoft.com/keys/microsoft.asc && \
sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo' && \
sed -i 's/#AutomaticUpdatePolicy.*/AutomaticUpdatePolicy=stage/' /etc/rpm-ostreed.conf && \
systemctl enable rpm-ostreed-automatic.timer && \
rpm-ostree cleanup -m && \
ostree container commit

FROM base

COPY justfile /tmp/build/ublue-os-just/justfile

# Install necessary packages
RUN sed -i 's@enabled=0@enabled=1@g' /etc/yum.repos.d/rpmfusion-{,non}free{,-updates}.repo && \
rpm-ostree install python3-pip steam-devices wireguard-tools code && \
sed -i 's@enabled=1@enabled=0@g' /etc/yum.repos.d/rpmfusion-{,non}free{,-updates}.repo && \
rpm-ostree cleanup -m && \
ostree container commit