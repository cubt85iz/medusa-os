set-kargs:
  rpm-ostree kargs \
    --append=rd.driver.blacklist=nouveau \
    --append=modprobe.blacklist=nouveau \
    --append=nvidia-drm.modeset=1

install-flatpaks:
  flatpak install flathub \
    com.brave.Browser \
    com.discordapp.Discord \
    org.gimp.GIMP \
    org.gnome.Boxes \
    com.heroicgameslauncher.hgl \
    com.github.iwalton3.jellyfin-media-player \
    org.libreoffice.LibreOffice \
    com.makemkv.MakeMKV \
    org.bunkus.mkvtoolnix-gui \
    tv.plex.PlexDesktop \
    org.libretro.RetroArch \
    com.valvesoftware.Steam \
    org.videolan.VLC

install-firefox-flatpak:
  flatpak override \
    --user \
    --filesystem=host-os \
    --env=LIBVA_DRIVER_NAME=nvidia \
    --env=LIBVA_DRIVERS_PATH=/run/host/usr/lib64/dri \
    --env=LIBVA_MESSAGING_LEVEL=1 \
    --env=MOZ_DISABLE_RDD_SANDBOX=1 \
    --env=NVD_BACKEND=direct \
    org.mozilla.firefox